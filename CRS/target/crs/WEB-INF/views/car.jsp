<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="com.nss.crs.model.*,java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <link rel="stylesheet" type="text/css" href="style.css">
    <head><title>Home Page</title></head>
    <body>
        <h1> 
            <b>Car Information.</b>  
        </h1>
        <table border="3">
            <tbody>
                <tr>
                    <td>ID</td><td><c:out value="${car.getId()}" /></td>
                </tr>
                <tr>
                    <td>Company</td><td><c:out value="${car.getCompanyManufacturer()}" /></td>
                </tr>
                <tr>
                    <td>Model</td><td><c:out value="${car.getModel()}" /></td>
                </tr>
                <tr>
                    <td>Body Type</td><td><c:out value="${car.getBodyType()}" /></td>
                </tr>
                <tr>
                    <td>Order Numbers</td><td><c:out value="${car.getCarNumber()}" /></td>
                </tr>
                <tr>
                    <td>Date Construction</td><td><fmt:formatDate pattern="yyyy-MMM-dd" 
                                    value="${car.getDateConstruction()}" /></td>
                </tr>
            </tbody>
        </table>
        <strong>
            <a href="CarInfo?action=insert&carId=${car.getId()}">Edit</a>
        </strong>
    </body>
</html>