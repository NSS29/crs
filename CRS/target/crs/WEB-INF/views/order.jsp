<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="com.nss.crs.model.*,java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <link rel="stylesheet" type="text/css" href="style.css">
    <head><title>Order Page</title></head>
    <body>
    <center>
        <div>
            <h1> 
                <b>Order Information.</b>  
            </h1>
            <table border="3">
                <tbody>
                    <tr>
                        <td>ID</td><td><c:out value="${order.getId()}" /></td>
                    </tr>
                    <tr>
                        <td>First Rental Day</td><td><fmt:formatDate pattern="yyyy-MMM-dd"
                                        value="${order.getFirstRentalDay()}" /></td>
                    </tr>
                    <tr>
                        <td>last Rental Day</td><td><fmt:formatDate pattern="yyyy-MMM-dd"
                                        value="${order.getLastRentalDay()}" /></td>
                    </tr>
                    <tr>
                        <td>Car</td><td><c:out value="${order.getCar()}" /></td>
                    </tr>
                </tbody>
            </table>
            <a href="CarInfo">Display all cars</a>
        </div>
    </center>
</body>
</html>