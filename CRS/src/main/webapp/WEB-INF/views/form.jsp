<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>	
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Form Page</title>
    </head>
    <body>
    <center>
        <div id="mystyle" class="myform">
            <form id="form" name="form" method="POST" action="OrderInfo">
                <h1>Insertation</h1>
                <p>Please enter the following information</p>
                
                <label>Id
                    <span class="small">Enter order's id</span>
		</label>
                <input type="text" name="id" id="id" />
                
                <label>First Rental Day
                    <span class="small">Enter From</span>
		</label>
		<input type="text" name="firstDate" id="firstDate" />
                
                <label>Last Rental Day
                    <span class="small">Enter To</span>
		</label>
		<input type="text" name="lastDate" id="lastDate" />
                
                <button type="submit">Submit</button>
		<div class="spacer"></div>
                
            </form>
        </div>
    </center>
    </body>
</html>