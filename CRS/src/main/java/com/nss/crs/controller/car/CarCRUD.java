////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.controller.car;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.*;
import com.nss.crs.persistence.*;
import com.nss.crs.persistence.impl.*;
import com.nss.crs.persistence.service.DAOFactory;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.*;
import javax.servlet.*;
import javax.servlet.http.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class CarCRUD extends HttpServlet {
    
    private static String LIST = "/WEB-INF/views/list.jsp";
    private static String EDIT = "/WEB-INF/views/car.jsp";
    private static String INSERT= "/WEB-INF/views/insert.jsp";
    
    private CarDAO cDao;
    private OrderDAO oDao;
    
    public CarCRUD() {
        super();
        Properties props = new Properties();
        InputStream istream = getClass().getClassLoader().getResourceAsStream(
                "config.properties");
	try {
            props.load(istream);
	} catch (IOException ioe) {
            Logger.getLogger(CarCRUD.class.getName()).log(Level.SEVERE, null, ioe);
	}
        String key = props.getProperty("dao.type");
        
        cDao=DAOFactory.getDAOFactory(key).getCarDAO();
        oDao=DAOFactory.getDAOFactory(key).getOrderDAO();
        Car car_1=new Car(1L, "BMW", "325iM", "sedan", "AB3456BA",
                    new Date());
        Car car_2=new Car(2L, "BMW", "525iM", "sedan", "AB3446BA",
                    new Date());
        Car car_3=new Car(3L, "BMW", "325iM", "sedan", "AB3656BA",
                    new Date());
        Car car_4=new Car(4L, "BMW", "750", "sedan", "AB7777BA",
                    new Date());
        
        Car[] cars = {car_1, car_2, car_3, car_4};
        
        for(Car car:cars)
            cDao.insert(car);
    }

    
//------------------------------------------------------------------------------
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");
        if(action!=null) {
            if(action.equalsIgnoreCase("delete")) {
                Long id = Long.parseLong(request.getParameter("carId"));
                cDao.delete(cDao.findEntityByID(id));
                forward=LIST;
                request.setAttribute("listCar", cDao.findAll());
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                Long id = Long.parseLong(request.getParameter("carId"));
                Car car=cDao.findEntityByID(id);
                request.setAttribute("car", car);
            } else if (action.equalsIgnoreCase("list")) {
                forward =LIST;
                request.setAttribute("listCar", cDao.findAll());
            } else
                forward = INSERT;
        }
        else {
            forward = LIST;
            request.setAttribute("listCar", cDao.findAll());
        }
        request.getRequestDispatcher(forward).forward(request, response);
    }
//------------------------------------------------------------------------------
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Car car=new Car();
        String id = request.getParameter("id");
	String company = request.getParameter("company");
	String model = request.getParameter("model");
	String body = request.getParameter("body");
	String number = request.getParameter("number");
        String date = request.getParameter("date");
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        
        car.setId(Long.parseLong(id));
        car.setCompanyManufacturer(company);
        car.setModel(model);
        car.setBodyType(body);
        car.setCarNumber(number);
        try {
            car.setDateConstruction(format.parse(date));
        } catch (ParseException ex) {
            Logger.getLogger(CarCRUD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        cDao.insert(car);
        
        request.setAttribute("listCar", cDao.findAll());
        request.getRequestDispatcher(LIST).forward(request, response);
    }
}
////////////////////////////////////////////////////////////////////////////////