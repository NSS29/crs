////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence.impl;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.model.*;
import com.nss.crs.persistence.*;
import com.nss.crs.persistence.store.CollectionStore;
import java.util.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class OrderCollectionDAOImpl extends OrderDAO{

    private List<Order> listOrder;
    
    public OrderCollectionDAOImpl() {
        listOrder=CollectionStore.INSTANCE.getOrderStore();
    }
    
    @Override
    public List<Order> findAll() {
        return listOrder;
    }

    @Override
    public Order findEntityByID(Long id) {
        for(Order order:listOrder)
            if(order.getId() == id)
                return order;
        return null;
    }

    @Override
    public boolean insert(Order order) {
        if(listOrder.contains(order))
            return false;
        return listOrder.add(order);
    }

    @Override
    public Order update(Order order) {
        for(Order o:listOrder)
            if(o.equals(order)) {
                o.setId(order.getId());
                o.setFirstRentalDay(order.getFirstRentalDay());
                o.setLastRentalDay(order.getLastRentalDay());
                o.setCar(order.getCar());
                return o;
            }
        return null;              
    }

    @Override
    public boolean delete(Order order) {
        return listOrder.remove(order);
    }

    @Override
    public Set<Order> findOrdersByCar(Car car) {
        Set<Order> setOrder=new LinkedHashSet();
        for(Order order:listOrder)
            if(order.getCar().equals(car))
                setOrder.add(order);
        return setOrder;
    }
}
////////////////////////////////////////////////////////////////////////////////