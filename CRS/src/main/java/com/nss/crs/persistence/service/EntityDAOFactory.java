////////////////////////////////////////////////////////////////////////////////
package com.nss.crs.persistence.service;
////////////////////////////////////////////////////////////////////////////////
import com.nss.crs.persistence.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public abstract class EntityDAOFactory {
    public abstract CarDAO getCarDAO();
    public abstract OrderDAO getOrderDAO();
}
////////////////////////////////////////////////////////////////////////////////